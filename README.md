# Labs nfs-server 
-----

## Entorno
- SO: Ubuntu 20.04

## Requisitos  
- Instalacion de Virtualbox
- Instalacion de Vagrant

## Informacion maquina virtuales
-----
### NfsServer (nfs-server)

| Maquina Virtual | nfsServer (nfs-server)                                              |
| --------------- | ------------------------------------------------------------------- |
| SO              | Centos 7                                                            |
| Ip              | 192.168.0.20                                                        |
| **Notas**       | **Revisar la configuracion de red para vuestro caso en particular** |

### NfsClient (nfs-client)

| Maquina Virtual | nfsClient (nfs-client)                                              |
| --------------- | ------------------------------------------------------------------- |
| SO              | Centos 7                                                            |
| Ip              | 192.168.0.30                                                        |
| **Notas**       | **Revisar la configuracion de red para vuestro caso en particular** | 

## Step 1: Verificacion de requisitos  
------

```
$ virtualbox --help
```
```
Oracle VM VirtualBox VM Selector v6.1.16
(C) 2005-2020 Oracle Corporation
All rights reserved.

No special options.

If you are looking for --startvm and related options, you need to use VirtualBoxVM.
```

```
$ vagrant --version
```

```
Vagrant 2.2.9
```

## Step 2: Recursos

```
$ git clone git@gitlab.com:cleandev-devops-labs/nfs-server.git
$ cd nfs-server
$ vagrant up
```
## Step 3: Scripts

**Ingresamos en la maquina nfsServer y ejecutamos el script de instalacion**
```
$ vagrant ssh nfsServer
$ ./init-nfs-server.sh
```
**Ingresamos en la maquina nfsClient y ejecutamos el script de instalacion**
```
$ vagrant ssh nfsClient
$ ./init-nfs-client.sh
```

## Shortcuts  
#### Maquinas Vagrant
```
$ vagrant global-status
```

#### Eliminar VM
``` 
$ vagrant destroy [nfsServer|nfsClient]
```
#### Iniciar VMs

**Nota**: En el directorio donde raiz del proyecto
```
$ vagrant up
```

#### Reiniciar el servidor nfs
``` sh  
$ sudo systemctl restart nfs-server
```

#### Montar volumen desde el cliente
```
$ showmount -e 192.168.0.20
$ mount -t nfs 192.168.0.20:/xxx /xxxx
```
