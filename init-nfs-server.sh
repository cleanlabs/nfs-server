#!/bin/bash

# Instalando dependencias
sudo yum update -y
sudo yum install vim -y
sudo yum install net-tools -y
sudo yum install nfs-utils -y

# Creando estructura de directorios
sudo mkdir -p /NFS/lucual18/docker/pre/
sudo chmod -R 777 /NFS/

# Inicializando servicios
sudo systemctl start rpcbind
sudo systemctl start nfs-server
sudo systemctl start nfs-lock
sudo systemctl start nfs-idmap
sudo systemctl enable rpcbind
sudo systemctl enable nfs-server

# Creando archivo de configuracion 
# Creando archivo de configuracion
sudo touch /etc/exports
sudo chown vagrant:vagrant /etc/exports
echo "/NFS/lucual18/docker/pre 192.168.0.30/24(rw,no_root_squash,no_all_squash,no_subtree_check)" > /etc/exports
sudo chown root:root /etc/exports



# Recargando configuracion servidor nfs
sudo systemctl restart nfs-server
