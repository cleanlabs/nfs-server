#!/bin/bash

# Instalando dependencias
sudo yum update -y
sudo yum install vim -y
sudo yum install net-tools -y
sudo yum install nfs-utils -y

# Creando estructura de directorios
sudo mkdir -p /NFS/lucual18/docker/pre/
sudo mkdir -p /mnt/lucual18/docker/pre/
sudo chmod -R 777 /NFS/

# Inicializando servicios
sudo systemctl start rpcbind
sudo systemctl start nfs-server
sudo systemctl start nfs-lock
sudo systemctl start nfs-idmap
sudo systemctl enable rpcbind
sudo systemctl enable nfs-server

